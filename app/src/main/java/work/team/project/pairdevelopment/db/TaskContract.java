package work.team.project.pairdevelopment.db;

import android.provider.BaseColumns;

public class TaskContract {
    public static final String DB_NAME = "work.team.project.pairdevelopment.db";
    public static final int DB_VERSION = 1;


    /*
    * Хийхээр төлөвлөж буй ажлын дарааллыг өгөгдлийн санд table үүсгэж хадгалахад ашиглагдах TaskEntry
    * дотоод (inner) класс.
     */
    public class TaskEntry implements BaseColumns {
        public static final String TABLE = "tasks";
        public static final String COL_TASK_TITLE = "title";
    }
}
