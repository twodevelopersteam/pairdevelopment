package work.team.project.pairdevelopment.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TaskDBHelper extends SQLiteOpenHelper {

    public TaskDBHelper(Context context) {
        super(context, TaskContract.DB_NAME, null, TaskContract.DB_VERSION);
    }
    /*
     * onCreate функц нь энэ классыг ажлуухад хамгийн түрүүнд ажиллах класс ба
     *String төрөлтэй createTable хувьсагчид TABLE үүсгэх кодуудыг хадгална
     * TABLE -ийн нэр нь tasks
     * ID дугаар автоматаар нэмэгддэг байх ба primary key болно (primary key нь энэхүү TABLE -д засвар хийх, устгах, өөр TABLE -тай
     * холбох ( нэгтгэх) -од ашиглахыг тогтоож өгдөг гол түлхүүр юм.)
     * Тухайн хийх гэж төлөвлөсөн ажлын гарчиг нь хоосон байж болохгүй.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + TaskContract.TaskEntry.TABLE + " ( " +
                TaskContract.TaskEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TaskContract.TaskEntry.COL_TASK_TITLE + " TEXT NOT NULL);";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TaskContract.TaskEntry.TABLE);
        onCreate(db);
    }
}
