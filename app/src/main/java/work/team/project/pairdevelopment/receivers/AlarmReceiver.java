package work.team.project.pairdevelopment.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.provider.CalendarContract;
import android.support.v4.content.WakefulBroadcastReceiver;

import work.team.project.pairdevelopment.model.Reminder;
import work.team.project.pairdevelopment.model.ReminderDataBase;

public class AlarmReceiver extends WakefulBroadcastReceiver{

    AlarmManager mAlarmManager;
    PendingIntent mPendinIntent;

    @Override
    public void onReceive(Context context, Intent intent) {
        int mReceivedID = Integer.parseInt(intent.getStringExtra(ReminderEditActivity.EXTRA_REMINDER_ID));
        //Өгөгдлийн сангаас notification ны гарчигийг өгөгдлийн сангаас авах
        ReminderDatabase rb = new ReminderDataBase(context);
        Reminder reminder = rb.getReminber(mReceivedID);
    }
}
