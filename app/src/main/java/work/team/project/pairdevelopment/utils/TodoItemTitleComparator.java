package work.team.project.pairdevelopment.utils;

import java.util.Comparator;

import work.team.project.pairdevelopment.model.Reminder;

public class TodoItemTitleComparator implements Comparator<Reminder> {
    @Override
    public int compare(Reminder one, Reminder another) {
        return one.title.compareTo(another.title);
    }
}
