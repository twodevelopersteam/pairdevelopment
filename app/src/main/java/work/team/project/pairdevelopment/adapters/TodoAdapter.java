package work.team.project.pairdevelopment.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import work.team.project.pairdevelopment.R;
import work.team.project.pairdevelopment.model.Reminder;

public class TodoAdapter extends ArrayAdapter<Reminder> {
    private Context mContext;
    private String mCurrentTime;

    /*
    *   Классын байгуулагч
    *   Энэхүү байгуулагч 3 параметртэй ба гишүүн өгөгдлүүд болох
    *   mContext, mCurrentTime -д утга онооно
     */
    public TodoAdapter(@NonNull Context context, ArrayList<Reminder> items, String mCurrentTime) {
        super(context, 0, items);
        this.mContext = context;
        this.mCurrentTime = mCurrentTime;
    }

    /*
    *   Хуваарь бүрийн хувьд гарчиг, тайлбар, он сар, төлөвийн утгыг авах
    *   өөрөөр тайлбарлавал хуваарын жагсаалтаас дэлгэцэнд гаргахад бэлэн болгох
    *   функц
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Reminder item = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_todo, parent, false);
        }

        TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
        TextView tvBody = (TextView) convertView.findViewById(R.id.tv_body);
        TextView tvDate = (TextView) convertView.findViewById(R.id.tv_date);
        TextView tvStatus = (TextView) convertView.findViewById(R.id.tv_status);

        tvTitle.setText(item.title);
        tvTitle.setTextColor(setColor(item.priority));

        tvBody.setText(item.body);
        tvBody.setTextColor(ContextCompat.getColor(mContext, R.color.colorBody));

        tvDate.setText(item.dueDate);

        if(item.status == 2){
            tvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.colorDisable));
            tvBody.setTextColor(ContextCompat.getColor(mContext, R.color.colorDisable));
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        try{
            if(sdf.parse(item.dueDate).before(sdf.parse(mCurrentTime))){
                tvTitle.setTextColor(ContextCompat.getColor(mContext, R.color.colorDisable));
                tvBody.setTextColor(ContextCompat.getColor(mContext, R.color.colorDisable));
                item.status = 3;
                tvStatus.setText("Хугацаа дууссан");
            } else{
                tvStatus.setText("");
                if(item.status == 2){
                    tvStatus.setText("Хийж дууссан");
                    tvStatus.setTextColor(ContextCompat.getColor(mContext, R.color.colorDoneStatus));
                }
            }
        } catch (ParseException e){
            e.printStackTrace();
        }
        // бэлэн болсон View буюу ажлын хуваарын мэдээллийг дэлгэцэнд гаргахаар буцаана
        return convertView;
    }

    private int setColor(int priority){
        if(priority == 1){
            // Өндөр зэрэглэлтэй ажлын хуваарь улаанаар бичигдсэн байна
            return ContextCompat.getColor(mContext, R.color.colorPriorityHigh);
        }
        else if(priority == 2){
            // Дундаж зэрэглэлтэй ажлын хуваарь улбар шараар бичигдсэн байна
            return ContextCompat.getColor(mContext, R.color.colorPriorityMed);
        }
        else if(priority == 3){
            // Бага зэрэглэлтэй ажлын хуваарь ногооноор бичигдсэн байна
            return  ContextCompat.getColor(mContext, R.color.colorPriorityLow);
        }
        return -1;
    }
}

