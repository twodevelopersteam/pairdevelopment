package work.team.project.pairdevelopment.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;

import work.team.project.pairdevelopment.model.Reminder;

public class TodoItemDateComparator implements Comparator<Reminder> {

    
    @Override
    public int compare(Reminder one, Reminder another) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        try{
            if(sdf.parse(one.dueDate).before(sdf.parse(another.dueDate)))
                return -1;
            else if(sdf.parse(one.dueDate).after(sdf.parse(another.dueDate)))
                return 1;
        } catch (ParseException e){
            e.printStackTrace();
        }
        return 0;
    }
}
